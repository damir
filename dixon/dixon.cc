#include <myint.h>

myInt factor(myInt n) {
  myInt x, xx, d, root;

  root = sqrt(n);

  if (issquare(n))
    return root;

  x = root;

  for (;; x++) {
    d = gcd(x, n);

    if (1 < d && d < n)
      return d;

    xx = (x * x) % n;

    if (issquare(xx)) {
      d = gcd((x - sqrt(xx)) % n, n);

      if (1 < d && d < n)
        return d;
    }
  }
  cout << "couldn't find!" << endl;

  return 0;
};

int main(int argc, char *argv[]) {
  myInt n, f;

  if (argc < 2) {
    cerr << "usage: " << argv[0] << " number_to_factorize" << endl;
    return 1;
  }

  n = argv[1];

  if (n == 1) {
    cout << n << ": is " << n << endl;
    return 0;
  }

  cout << n << ": " << flush;

  if (isprime(n)) {
    cout << "is prime" << endl;
    return 0;
  }

  while (!isprime(n)) {
    f = factor(n);
    cout << f << " " << flush;
    n /= f;
  }
  cout << n << endl;

  return 0;
};

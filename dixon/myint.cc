#include <stdlib.h>
#include "myint.h"

myInt::myInt() {
  mpz_init_set_ui(_x, 0);
  base = 10;
}

myInt::myInt(int a) {
  mpz_init_set_si(_x, a);
  base = 10;
}

myInt::myInt(const myInt &a) {
  mpz_init_set(_x, a._x);
  base = a.base;
}

myInt::myInt(char *a) {
  mpz_init_set_str(_x, a, 10);
  base = 10;
}

myInt::myInt(mpz_t a) {
  mpz_init_set(_x, a);
  base = 10;
}

myInt::~myInt() {
  mpz_clear(_x);
}

myInt::operator bool(void) {
  return (bool)(mpz_cmp_ui(_x, 0));
}
myInt::operator int(void) {
  return (int)mpz_get_si(_x);
}

myInt::operator float(void) {
  return (float)mpz_get_d(_x);
}
myInt::operator double(void) {
  return (double)mpz_get_d(_x);
}

void myInt::operator =(myInt a) {
  mpz_set(_x, a._x);
  base = a.base;
}

void myInt::operator =(int a) {
  mpz_set_si(_x, a);
  base = 10;
}

void myInt::operator =(char *a) {
  mpz_set_str(_x, a, 10);
  base = 10;
}

void myInt::operator =(const char *a) {
  mpz_set_str(_x, a, 10);
  base = 10;
}

void myInt::operator =(mpz_t a) {
  mpz_set(_x, a);
  base = 10;
}

myInt myInt::operator -(void) {
  myInt r;
  mpz_neg(r._x, _x);

  return r;
}

bool myInt::operator !(void) {
  return (mpz_cmp_ui(_x, 0) == 0);
}

void myInt::operator +=(myInt a) {
  mpz_add(_x, _x, a._x);
}

void myInt::operator +=(int a) {
  (a > 0)
    ? mpz_add_ui(_x, _x, a)
    : mpz_sub_ui(_x, _x, -a);
}

void myInt::operator -=(myInt a) {
  mpz_sub(_x, _x, a._x);
}

void myInt::operator -=(int a) {
  (a > 0)
    ? mpz_sub_ui(_x, _x, a)
    : mpz_add_ui(_x, _x, -a);
}

void myInt::operator *=(myInt a) {
  mpz_mul(_x, _x, a._x);
}

void myInt::operator *=(int a) {
  mpz_mul_ui(_x, _x, a);

  if (a < 0)
    mpz_neg(_x, _x);
}

void myInt::operator /=(myInt a) {
  mpz_tdiv_q(_x, _x, a._x);
}

void myInt::operator /=(int a) {
  mpz_tdiv_q_ui(_x, _x, a);

  if (a < 0)
    mpz_neg(_x, _x);
}

void myInt::operator %=(myInt a) {
  mpz_mod(_x, _x, a._x);
}

void myInt::operator %=(int a) {
  mpz_mod_ui(_x, _x, a);
}

myInt myInt::operator ++(void) {
  mpz_add_ui(_x, _x, 1);

  return _x;
}

myInt myInt::operator --(void) {
  mpz_sub_ui(_x, _x, 1);

  return _x;
}

myInt myInt::operator ++(int a) {
  myInt r = _x;
  mpz_add_ui(_x, _x, 1);

  return r;
}

myInt myInt::operator --(int a) {
  myInt r = _x;
  mpz_sub_ui(_x, _x, 1);

  return r;
}

myInt operator +(myInt a, myInt b) {
  myInt c;
  mpz_add(c._x, a._x, b._x);

  return c;
}

myInt operator +(myInt a, int b) {
  myInt c;

  if (b > 0)
    mpz_add_ui(c._x, a._x, b);
  else
    mpz_sub_ui(c._x, a._x, -b);

  return c;
}

myInt operator +(int a, myInt b) {
  return b + a;
}

myInt operator -(myInt a, myInt b) {
  myInt c;
  mpz_sub(c._x, a._x, b._x);

  return c;
}

myInt operator -(myInt a, int b) {
  return a + (-b);
}

myInt operator -(int a, myInt b) {
  return -(b - a);
}

myInt
operator *(myInt a, myInt b)
{
  myInt c;
  mpz_mul(c._x, a._x, b._x);
  return c;
}

myInt operator *(myInt a, int b) {
  myInt c;
  mpz_mul_ui(c._x, a._x, b);

  return (b < 0
          ? -c
          : c);
}

myInt operator *(int a, myInt b) {
  return b * a;
}

myInt operator /(myInt a, myInt b) {
  myInt c;
  mpz_tdiv_q(c._x, a._x, b._x);
  return c;
}

myInt operator /(myInt a, int b) {
  myInt c;
  mpz_tdiv_q_ui(c._x, a._x, b);

  return (b < 0
          ? -c
          : c);
}

myInt operator /(int a, myInt b) {
  myInt c;
  c = a;

  return c / b;
}

myInt operator %(myInt a, myInt b) {
  myInt c;
  mpz_mod(c._x, a._x, b._x);

  return c;
}

myInt operator %(myInt a, int b) {
  myInt c;

  mpz_mod_ui(c._x, a._x, b);
  return c;
}

myInt operator %(int a, myInt b) {
  myInt c;
  c = a;

  return c % b;
}

bool operator ==(myInt a, myInt b) {
  return (mpz_cmp(a._x, b._x) == 0);
}

bool operator ==(myInt a, int b) {
  return (mpz_cmp_si(a._x, b) == 0);
}

bool operator ==(int a, myInt b) {
  return (b == a);
}

bool operator !=(myInt a, myInt b) {
  return(mpz_cmp(a._x, b._x) != 0);
}

bool operator !=(myInt a, int b) {
  return (mpz_cmp_si(a._x, b) != 0);
}

bool operator !=(int a, myInt b) {
  return (b != a);
}

bool operator <(myInt a, myInt b) {
  return (mpz_cmp(a._x, b._x) < 0);
}

bool operator <(myInt a, int b) {
  return (mpz_cmp_si(a._x, b) < 0);
}

bool operator <(int a, myInt b) {
  return (b > a);
}

bool operator <=(myInt a, myInt b) {
  return (mpz_cmp(a._x, b._x) < 0);
}

bool operator <=(myInt a, int b) {
  return (mpz_cmp_si(a._x, b) < 0);
}

bool operator <=(int a, myInt b) {
  return(b >= a);
}

bool operator >(myInt a, myInt b) {
  return (mpz_cmp(a._x, b._x) > 0);
}

bool operator >(myInt a, int b) {
  return (mpz_cmp_si(a._x, b) > 0);
}

bool operator >(int a, myInt b) {
  return (b < a);
}

bool operator >=(myInt a, myInt b) {
  return (mpz_cmp(a._x, b._x) > 0);
}

bool operator >=(myInt a, int b) {
  return (mpz_cmp_si(a._x, b) > 0);
}

bool operator >=(int a, myInt b) {
  return (b <= a);
}

ostream &operator <<(ostream &s, myInt i) {
  char *p;
  p = (char *)new char[4 + mpz_sizeinbase(i._x, i.base) - 1];
  mpz_get_str(p, i.base, i._x);
  s << p;

  delete p;

  return s;
}

istream &operator >>(istream &s, myInt &a) {
  char *p;
  p = (char *)new char[8192];
  s >> p;
  a = (myInt)p;

  delete p;

  return s;
}

myInt gcd(myInt a, myInt b) {
  myInt g;
  mpz_gcd(g._x, a._x, b._x);

  return g;
}

bool isprime(myInt a) {
  return (bool)mpz_probab_prime_p(a._x, 25);
}

bool issquare(myInt a) {
  return (bool)mpz_perfect_square_p(a._x);
}

myInt sqrt(myInt a) {
  myInt r;
  mpz_sqrt(r._x, a._x);

  return r;
}

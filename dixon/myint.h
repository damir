#include <stdlib.h>

#ifndef __MYINT__
#define __MYINT__

#include <iostream.h>
#include <time.h>
#include <gmp.h>

class myInt {
 private:
  mpz_t _x;
  int base;

 public:
  myInt(void);
  myInt(int);
  myInt(const myInt &);
  myInt(char *);
  myInt(mpz_t);

  ~myInt(void);

  operator bool(void);
  operator int(void);
  operator float(void);
  operator double(void);

  void operator =(myInt);
  void operator =(int);
  void operator =(char *);
  void operator =(const char *);
  void operator =(mpz_t);

  myInt operator -(void);
  bool operator !(void);

  void operator +=(myInt);
  void operator +=(int);

  void operator -=(myInt);
  void operator -=(int);

  void operator *=(myInt);
  void operator *=(int);

  void operator /=(myInt);
  void operator /=(int);

  void operator %=(myInt);
  void operator %=(int);

  myInt operator ++(void);
  myInt operator --(void);

  myInt operator ++(int);
  myInt operator --(int);

  friend myInt operator +(myInt, myInt);
  friend myInt operator +(myInt, int);
  friend myInt operator +(int, myInt);

  friend myInt operator -(myInt, myInt);
  friend myInt operator -(myInt, int);
  friend myInt operator -(int, myInt);

  friend myInt operator *(myInt, myInt);
  friend myInt operator *(myInt, int);
  friend myInt operator *(int, myInt);

  friend myInt operator /(myInt, myInt);
  friend myInt operator /(myInt, int);
  friend myInt operator /(int, myInt);

  friend myInt operator %(myInt, myInt);
  friend myInt operator %(myInt, int);
  friend myInt operator %(int, myInt);


  friend bool operator ==(myInt, myInt);
  friend bool operator ==(myInt, int);
  friend bool operator ==(int, myInt);

  friend bool operator !=(myInt, myInt);
  friend bool operator !=(myInt, int);
  friend bool operator !=(int, myInt);

  friend bool operator <(myInt, myInt);
  friend bool operator <(myInt, int);
  friend bool operator <(int, myInt);

  friend bool operator <=(myInt, myInt);
  friend bool operator <=(myInt, int);
  friend bool operator <=(int, myInt);

  friend bool operator >(myInt, myInt);
  friend bool operator >(myInt, int);
  friend bool operator >(int, myInt);

  friend bool operator >=(myInt, myInt);
  friend bool operator >=(myInt, int);
  friend bool operator >=(int, myInt);

  friend ostream &operator <<(ostream &, myInt);
  friend istream &operator >>(istream &, myInt &);

  friend myInt gcd(myInt, myInt);
  friend bool isprime(myInt);
  friend bool issquare(myInt);
  friend myInt sqrt(myInt);
};
#endif

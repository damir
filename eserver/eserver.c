#include <errno.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>

void *worker(void *sock) {
  int dSock = *((int *)sock);
  size_t dataSent, dataToSend;
  char answer[256];
  int result;
  free(sock);

  while (1) {
    result = recv(dSock, answer, sizeof(answer), 0);

    if (result < 0) {
      if (errno ==  EINTR)
        continue;
      else {
        fprintf(stderr, "recv(): %s\n",  strerror(errno));
        break;
      }
    } else {
      if (result == 0) {
        break;
      } else {
        dataToSend = result;
        dataSent = 0;

        while (dataSent < dataToSend) {
          result = send(dSock,
                        answer + dataSent,
                        dataToSend - dataSent,
                        0);

          if (result < 0 && errno != EINTR) {
            fprintf(stderr, "send(): %s\n", strerror(errno));
            break;
          } else {
            dataSent += result;
          }
        }
        if (dataSent < dataToSend) {
          break;
        }
      }
    }
  }

  printf("Closed\n");
  close(dSock);

  return NULL;
}

int main(int argc, char *argv[]) {

  if (argc < 2) {
    printf("Usage: %s port\n", argv[0]);
    exit(1);
  }

  int lSock;

  struct sockaddr_in lSockAddr;

  if ((lSock = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
    fprintf(stderr, "socket(): %s\n", strerror(errno));
    abort();
  }

  memset(&lSockAddr, 0, sizeof(lSockAddr));
  lSockAddr.sin_family = PF_INET;
  lSockAddr.sin_port = htons(atoi(argv[1]));
  lSockAddr.sin_family = INADDR_ANY;

  int x = 1;

  if (setsockopt(lSock, SOL_SOCKET, SO_REUSEADDR, &x, sizeof(x)) < 0) {
    fprintf(stderr, "setsockopt(SO_REUSEADDR): %s\n", strerror(errno));
    abort();
  }

  if (bind(lSock,
           (struct sockaddr *)&lSockAddr,
           sizeof(lSockAddr)) < 0) {
    fprintf(stderr, "bind(): %s\n", strerror(errno));
    abort();
  }

  if (listen(lSock, 5) < 0) {
    fprintf(stderr, "listen(): %s\n", strerror(errno));
    abort();
  }

  pthread_attr_t wAttr;
  pthread_attr_init(&wAttr);
  pthread_attr_setdetachstate(&wAttr, PTHREAD_CREATE_DETACHED);

  while (1) {
    int *dSock = malloc(sizeof(int));
    struct sockaddr_in dSockAddr;
    socklen_t dSockAddrSize = sizeof(dSockAddr);
    pthread_t wThread;
    int errcode;

    if ((*dSock = accept(lSock,
                         (struct sockaddr *)&dSockAddr,
                         &dSockAddrSize)) < 0) {
      fprintf(stderr, "accept(): %s\n", strerror(errno));
      abort();
    }
    printf("Accepted\n");

    if ((errcode = pthread_create(&wThread,
                                  &wAttr,
                                  worker,
                                  dSock) != 0)) {
      fprintf(stderr, "pthread_create(): %s\n", strerror(errno));
      abort();
    }
  }
  return 0;
}
